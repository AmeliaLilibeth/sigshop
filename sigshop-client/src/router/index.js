import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/shop",
    name: "shop",
    component: () => import("../views/Shop.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/Admin.vue'),
    meta: { requireAuth: true }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const protectedRoutes = to.matched.some(record => record.meta.requireAuth);
  if (protectedRoutes && store.state.token === "") {
    next({ name: "login" });
  } else {
    next();
  }
});

export default router;
