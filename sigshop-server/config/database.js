const mongoose = require("mongoose");
// const uri = "mongodb://localhost:27017/sigshop";
// const uri2 = "mongodb://localhost:27017/sigshop_mirror";
const uri = "mongodb+srv://admin:admin@sigshop-9itqj.mongodb.net/test?retryWrites=true&w=majority";
const uri2 = "mongodb+srv://admin:admin@sigshopmirror-7gimn.mongodb.net/test?retryWrites=true&w=majority";

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
};

mongoose.main_conn = mongoose.createConnection(uri, options);
mongoose.mirror_conn = mongoose.createConnection(uri2, options);

module.exports = exports = mongoose;
