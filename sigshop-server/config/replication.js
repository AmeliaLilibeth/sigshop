const mongoose = require("mongoose");

import UserSchema from "../models/user";
import HShopSchema from "../models/history_shop";
import HAuthSchema from "../models/history_auth";

const schemas = [
    { name: UserSchema, table: 'User' },
    { name: HShopSchema, table: 'Shop' },
    { name: HAuthSchema, table: 'Register' },
    { name: HAuthSchema, table: 'Login' }
]

const replication = () => {
    schemas.map(async schema => {
        const main_model = mongoose.main_conn.model(schema.table, schema.name)
        const mirror_model = mongoose.mirror_conn.model(schema.table, schema.name)

        const main_data = await main_model.find();
        const mirror_data = await mirror_model.find();

        if(schema.table === 'User') { userVerification(main_data, mirror_data, main_model) }
        else { verification(main_data, mirror_data, main_model, schema.table) }
    })
    console.log('Se ha verificado de la base de datos');
}

const verification = async (main_data, mirror_data, main_model, table) => {
    try {
        if(mirror_data){
            mirror_data.map(async user => {
                if(!main_data){
                    await main_model.create(user);
                    console.log("Recuperacion de datos realizada");
                }else{
                    let new_register = {}
                    const verification = main_data
                        .map(u => {
                            return u.userId;
                        })
                        .indexOf(user.userId);
                    if(verification === -1){
                        if(table === 'Shop') {
                            new_register = {
                                userId: user.userId,
                                shop_date: user.shop_date,
                                products: user.products,
                                total: user.total,
                                name: user.name,
                                lastname: user.lastname,
                                email: user.email
                            };
                        } else {
                            new_register = {
                                userId: user.userId,
                                date: user.date,
                                email: user.email
                            };
                        }
                        await main_model.create(new_register);
                        console.log("Recuperacion de datos realizada");
                    }
                }
            })
        }
    } catch (error) {
        console.log("Ocurrio un error al recuperar la base de datos ", error);
    }
}

const userVerification = async (main_data, mirror_data, main_model) => {
    try {
      if (mirror_data) {
        mirror_data.map(async user => {
          if (!main_data) {
            await main_model.create(user);
            console.log("Recuperacion de datos realizada");
          } else {
            const verification = main_data
              .map(u => {
                return u.email;
              })
              .indexOf(user.email);
            if (verification === -1) {
              const new_user = {
                name: user.name,
                lastname: user.lastname,
                pass: user.pass,
                email: user.email
              };
              await main_model.create(new_user);
              console.log("Recuperacion de datos realizada");
            }
          }
        });
      }
    } catch (error) {
      console.log("Ocurrio un error al recuperar la base de datos ", error);
    }
};

let minutes = 2, the_interval = minutes * 60 * 1000;
replication();
setInterval(async()=>{
    await replication();
}, the_interval);