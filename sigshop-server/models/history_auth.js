import mongoose from "mongoose";

const Schema = mongoose.Schema;

//Crear schema con los campos
const historyAuthSchema = new Schema({
  userId: {
    type: String,
    required: [true, "Usuario requerido"]
  },
  date: {
    type: Date,
    default: Date.now()
  },
  email: {
    type: String,
    required: [true, "Correo requerido"]
  },
});

module.exports = historyAuthSchema;
