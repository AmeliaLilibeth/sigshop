import mongoose from "mongoose";

const Schema = mongoose.Schema

var productSchema = new Schema({ 
  id: String ,
  title: String,
  price: String
});;

//Crear schema con los campos
const historyShopSchema = new Schema({
  userId: {
    type: String,
    required: [true, "ID requerido"]
  },
  name: {
    type: String,
    required: [true, "Nombre requerido"]
  },
  lastname: {
    type: String,
    required: [true, "Apellido requerido"]
  },
  email: {
    type: String,
    required: [true, "Correo requerido"]
  },
  shop_date: {
    type: Date,
    default: Date.now()
  },
  products: {
    type: Array,
    default: [],
    required: [true, "Productos requerido"]
  },
  total: {
    type: String,
    required: [true, "Total requerido"]
  },
});

module.exports = historyShopSchema;
