import mongoose from "mongoose";

const Schema = mongoose.Schema;

//Crear schema con los campos
const userSchema = new Schema({
  name: {
    type: String,
    required: [true, "Nombre requerido"]
  },
  lastname: {
    type: String,
    required: [true, "Apellido requerido"]
  },
  email: {
    type: String,
    required: [true, "Correo requerido"]
  },
  pass: {
    type: String,
    required: [true, "Contraseña requerida"]
  },
  active: {
    type: Boolean,
    default: true
  }
});

module.exports = userSchema;

// Convertir a modelo
// const User = mongoose.model("User", userSchema);

// export default User;
