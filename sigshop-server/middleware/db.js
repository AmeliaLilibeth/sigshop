import UserSchema from "../models/user";
const mongoose = require("mongoose");

const User = mongoose.main_conn.model("User", UserSchema);
const User2 = mongoose.mirror_conn.model("User", UserSchema);

const dbverification = async (req, res, next) => {
  try {
    const userDB1 = await User.find();
    const userDB2 = await User2.find();
    if (userDB2) {
      userDB2.map(async user2 => {
        if (!userDB1) {
          await User.create(user2);
          console.log("Recuperacion realizada con éxito");
        } else {
          const verification = userDB1
            .map(u => {
              return u.email;
            })
            .indexOf(user2.email);
          if (verification === -1) {
            const new_user = {
              name: user2.name,
              lastname: user2.lastname,
              pass: user2.pass,
              email: user2.email
            };
            await User.create(new_user);
            console.log("Recuperacion realizada con éxito");
          }
        }
      });
    }
    next();
  } catch (error) {
    console.log("Ocurrio un error al recuperar la base de datos", error);
  }
};

module.exports = {
  dbverification
};
