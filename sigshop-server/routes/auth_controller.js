const mongoose = require("mongoose");

import AuthSchema from '../models/history_auth';

const Login = mongoose.main_conn.model("Login", AuthSchema);
const Login2 = mongoose.mirror_conn.model("Login", AuthSchema);
const Register = mongoose.main_conn.model("Register", AuthSchema);
const Register2 = mongoose.mirror_conn.model("Register", AuthSchema);

const controller = {}

controller.addLogin = async (user) => {
  try {
    await Login.create(user);
    await Login2.create(user);
  } catch (error) {
    console.log(error);
  }
}

controller.addRegister = async (user) => {
  try {
    await Register.create(user);
    await Register2.create(user);
  } catch (error) {
    console.log(error);
  }
}

module.exports = controller;
