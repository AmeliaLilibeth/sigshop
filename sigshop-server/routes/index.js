const express = require("express");

import products from "./products";
import categories from "./categories";
import auth from "./auth";
import history from './history';

const router = express.Router();

router.use("/auth", auth);
router.use("/products", products);
router.use("/categories", categories);
router.use("/history", history);

module.exports = router;
