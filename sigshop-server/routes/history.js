const express = require("express");
const mongoose = require("mongoose");

import ShopSchema from '../models/history_shop';
import AuthSchema from '../models/history_auth';

const Login = mongoose.main_conn.model("Login", AuthSchema);
const Register = mongoose.main_conn.model("Register", AuthSchema);
const Shop = mongoose.main_conn.model("Shop", ShopSchema);
const Shop2 = mongoose.mirror_conn.model("Shop", ShopSchema);

const { verification } = require("../middleware/auth");

const router = express.Router();

router.post("/shop/add", verification, async (req, res) => {
    const body = req.body;
    body.products = Object.values(body.products);
    try {
      const shopDB = await Shop.create(body);
      const shopBU = await Shop2.create(body);
      res.json(shopDB);
    } catch (error) {
      return res.status(500).json({
        message: "Ocurrio un error",
        error
      });
    }
});

router.get("/shop/all", verification, async (req, res) => {
    try {
        const shopDB = await Shop.find();
        res.json(shopDB);
      } catch (error) {
        return res.status(400).json({
          mensaje: 'Ocurrio un error',
          error
        })
      }
});

router.get("/login/all", verification, async (req, res) => {
    try {
        const loginDB = await Login.find();
        res.json(loginDB);
      } catch (error) {
        return res.status(400).json({
          mensaje: 'Ocurrio un error',
          error
        })
      }
});

router.get("/register/all", verification, async (req, res) => {
    try {
        const registerDB = await Register.find();
        res.json(registerDB);
      } catch (error) {
        return res.status(400).json({
          mensaje: 'Ocurrio un error',
          error
        })
      }
});

module.exports = router;
