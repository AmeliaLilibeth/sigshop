import express from "express";
import axios from "axios";

const { verification } = require("../middleware/auth");

const router = express.Router();

router.get("/all", verification, async (req, res) => {
  try {
    const response = await axios.get(
      "https://api.mercadolibre.com/sites/MEC/categories"
    );
    res.json(response.data);
  } catch (error) {
    return res.status(400).json({
      message: "Ocurrio un error",
      error
    });
  }
});

module.exports = router;
