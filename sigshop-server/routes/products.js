const express = require("express");
import axios from "axios";

const { verification } = require("../middleware/auth");

const router = express.Router();

router.get("/all/:category", verification, async (req, res) => {
  const category = req.params.category;
  try {
    const response = await axios.get(
      `https://api.mercadolibre.com/sites/MEC/search?category=${category}`
    );
    res.json(response.data);
  } catch (error) {
    return res.status(400).json({
      mensage:
        "The server could not understand the request due to invalid syntax.",
      error
    });
  }
});

module.exports = router;
