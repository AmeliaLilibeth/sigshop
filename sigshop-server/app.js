import express from "express";
import morgan from "morgan";
import cors from "cors";
import path from "path";

const app = express();

// Middleware
app.use(morgan("tiny"));
app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true
  })
);

require('./config/database');

// Rutas del server
app.use("/api", require("./routes"));

// Middleware para Vue.js router modo history
const history = require("connect-history-api-fallback");
app.use(history());
app.use(express.static(path.join(__dirname, "public")));

require('./config/replication');

app.set("port", process.env.PORT || 3000);
app.listen(app.get("port"), () => {
  console.log("Listening on port: " + app.get("port"));
});
